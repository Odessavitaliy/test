(function() {
	'use strict';

	/* Init scripts */
	$(function() {
		/* sidebar drop menu */
		$('.list-group__list').on('click', function (e) {
			e.preventDefault();

			var $listGroupList = $('.list-group__list');

			if($(this).hasClass('_js-drop')){
                $(this).removeClass('_js-drop');
                $(this).next('.list-group__drop').slideUp();

			} else {
				$listGroupList.removeClass('_js-drop');
				$listGroupList.next('.list-group__drop').hide();
                $(this).addClass('_js-drop');
                $(this).next('.list-group__drop').slideDown();
			}
        });
		/* sidebar drop menu end */

		/* custom scroll in table init */
		$('.js-custom-scroll').mCustomScrollbar();
        $('.js-custom-scroll-sidebar').mCustomScrollbar();
		/* custom scroll in table init end */
		
		/* sidebar menu toggle */
		$('.js-btn-menu-toggle').on('click', function () {
		    var $filterBtn = $('.filter__btn');
			var $listGroup = $('.sidebar__list-group');

			$('body').toggleClass('_js-open-menu');
			$listGroup.toggleClass('js-custom-scroll-sidebar');

			if($listGroup.hasClass('js-custom-scroll-sidebar')){
				$('.js-custom-scroll-sidebar').mCustomScrollbar();
            } else {
				$('.js-custom-scroll-sidebar').mCustomScrollbar('destroy');
				$('.js-custom-scroll').mCustomScrollbar();
            }
			$('.filter-drop').fadeOut();
			$filterBtn.removeClass('active');
			$filterBtn.find('.filter__icon').removeClass('icon-plus');
			$filterBtn.find('.filter__icon').addClass('icon-filter');
        });
		/* sidebar menu toggle end */

		/* calendar datepicker */
        var filterBtn = $('.calendar__link'),
            calenndar = $('.calendar__datepicker'),
            closeBtn = $('.datepicker__cancel'),
            okBtn = $('.datepicker__btn');

        function toggleCalendarShow() {
            filterBtn.toggleClass('_js-arrow-up');
            calenndar.toggleClass('_js-datepicker-open');
        }
        okBtn.on('click', function (e) {
            e.preventDefault();
            toggleCalendarShow();
        });
        closeBtn.on('click', function (e) {
            e.preventDefault();
            toggleCalendarShow();
        });
        filterBtn.on('click', function (e) {
            e.preventDefault();
            toggleCalendarShow();
        });
        /* calendar datepicker end */

        /* custom select */
        $('.select p').on('click', function () {
            $(this).toggleClass('active');
            $(this).next('.ul').slideToggle('100');
        });

        $('.select .ul .li').on('click', function () {
            var $selectDropRowResult = $(this).html();
            $(this).parent('.ul').children('.li').show();
            $(this).hide();
            $(this).parent('.ul').prev().html($selectDropRowResult);
            $(this).parent('.ul').prev().toggleClass('active');
            $(this).parent().slideToggle('100');
        });
        /* custom select end */

        /* close custom select */
        $(document).on('click', function(e) {
            if (!$(e.target).closest(".select").length) {
                $('.select p').removeClass('active');
                $('.select').find('.ul').slideUp('100');
            }
            e.stopPropagation();
        });
        /* close custom select end */

        /* header select */
        $('.user').on('click', function () {
            $(this).find('.user__drop ').toggleClass('_drop');
            $(this).find('.user__select--arrow ').toggleClass('_drop');
        });
        $(document).on('click', function(e) {
            if (!$(e.target).closest(".user").length) {
                $('.user__drop ').removeClass('_drop');
                $('.user__select--arrow ').removeClass('_drop');
            }
            e.stopPropagation();
        });
        /* header select end */

        /* function isFilter init */
        isFilter();
        /* function isFilter init */
	});

	/* Init scripts after load the document */
	$(window).on("resize", function() {
        isCalcWidth();
	});

	/* Init scripts after load document and resize window width*/
	$(window).on("load resize", function() {

	});
})();

var calendarVidget = (function () {
    pickmeup('.three-calendars', {
        flat: true,
        mode: 'range',
        calendars: 2
    });
    pickmeup('input', {
        position: 'right',
        hide_on_select: true
    });
    document.querySelector('.three-calendars').addEventListener('pickmeup-change', function () {
        setTimeout(function () {
            var btnSelected = document.querySelectorAll('.pmu-days .pmu-selected');
            btnSelected[0].classList.add('pmu-first-last');
            btnSelected[btnSelected.length - 1].classList.add('pmu-first-last');
        }, 100)
    });
})(document);

/* function isCalcWidth  */
function isCalcWidth() {
    var $width = $('.table__row').innerWidth();
    var $filterDrop = $('.filter-drop');
    $filterDrop.css({'width': $width});
}
/* function isCalcWidth  end */

/* function isFilter */
function isFilter() {
    $('.filter__btn').on('click', function (e) {
        e.preventDefault();

        var $this = $(this);
        var $width = $('.table__row').innerWidth();
        var $filterDrop = $('.filter-drop');

        $filterDrop.css({'width': $width});
        $filterDrop.fadeToggle();

        $this.toggleClass('active');

        if($this.hasClass('active')){
            $this.find('.filter__icon').removeClass('icon-filter');
            $this.find('.filter__icon').addClass('icon-plus');
        } else {
            $this.find('.filter__icon').addClass('icon-filter');
            $this.find('.filter__icon').removeClass('icon-plus');
        }
    });

    $('.filter-drop__toggle').on('click', function (e) {
        e.preventDefault();

        var $this = $(this);

        $this.toggleClass('active');
        $('.filter-drop__row:last').slideToggle();

        if($this.hasClass('active')){
            $this.find('span').text('More');
        } else {
            $this.find('span').text('Less');
        }
    })
}
/* function isFilter end */